= Apache Arrow Flight SQLでPostgreSQLをもっと速く！

PostgreSQLとの接続には独自プロトコルが使われていますが、やりとりするデータが大きくなるとクエリーの処理ではなくこのプロトコルがボトルネックになることが知られています。Apache ArrowFlight SQLプロトコルはこのボトルネックを解消できるプロトコルです。Apache Arrow Flight SQLの詳細、どのくらい速くなるのか、プロトコルを拡張する仕組みのないPostgreSQLでどのように実装したのかといった実装の詳細を紹介します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者：須藤功平

====  株式会社クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-postgresql-conference-japan-2023

=== 表示

  rabbit rabbit-slide-kou-postgresql-conference-japan-2023.gem

